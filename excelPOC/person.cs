﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;
using System.Data;
using Excel;

namespace excelPOC
{
    class person
    {

        private string mName;
        private string mGender;
        private int mAge;

        public person(string aName, string aGender, int aAge)
        {
            mName = aName;
            mGender = aGender;
            mAge = aAge;
        }

        public override string ToString()
        {
            return String.Join(",", mName, mGender, mAge);
        }

        public static List<person> makePeople(string filename)
        {
            FileStream stream = File.Open(filename, FileMode.Open, FileAccess.Read);

            IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

            List<person> toReturn = new List<person>();

            // first row (titles)
            excelReader.Read();

            while(excelReader.Read())
            {
                // Rows 1, 2, 3
                toReturn.Add(new person(excelReader.GetString(0), excelReader.GetString(1), excelReader.GetInt32(2)));
            }

            excelReader.Close();

            return toReturn;
        }

    }
}
